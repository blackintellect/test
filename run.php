<?php

include 'ConsoleApp.php';

$app = new ConsoleApp();
$app->loadData();
$app->createReport();
$app->printResult();