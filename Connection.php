<?php

/**
 * Created by PhpStorm.
 * User: chuchu
 * Date: 20.02.2017
 * Time: 18:25
 */

include 'dataSource.php';

class Connection extends DataSource
{
    private $base;

    function __construct()
    {
        if ($this->getDatabaseConnection() === null) {
            $this->loadFixtures();
        }
    }

    public function start()
    {
        if ($this->base == null) {
            $this->base = $this->getDatabaseConnection();
        }

        return $this->base;
    }
}