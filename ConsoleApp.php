<?php

/**
 * Created by PhpStorm.
 * User: chuchu
 * Date: 20.02.2017
 * Time: 18:22
 */

include 'Connection.php';
include 'printable.php';

class ConsoleApp implements Printable
{
    private $data;
    private $report;

    public function loadData()
    {
        $connection = new Connection();
        $query = "SELECT strftime('%m.%Y', payments.create_ts) AS Date, COUNT(*) AS Count, sum(payments.amount) AS TotalAmount
                  FROM payments LEFT JOIN documents 
                  ON payments.id = documents.payment_id
                  WHERE documents.payment_id IS NULL
                  GROUP BY Date";
        $this->data = $connection->start()->query($query);
    }

    public function createReport()
    {
        foreach ($this->data as $row) {
            extract($row);
            /**
             * @var $Date
             * @var $Count
             * @var $TotalAmount
             */
            $this->report[] = "$Date\t$Count\t$TotalAmount\n";
        }
    }

    public function printResult()
    {
        foreach ($this->report as $row) {
            print_r($row);
        }
    }

}